import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

public class ImportCSV extends JFrame {

	private JPanel contentPane;
	private JTextField txtBoxCSVDirectory;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ImportCSV frame = new ImportCSV();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ImportCSV() {
		setTitle("Countdown Timer");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 330, 370);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnImportCSV = new JButton("Import");
		btnImportCSV.setBounds(113, 241, 84, 30);
		contentPane.add(btnImportCSV);
		
		txtBoxCSVDirectory = new JTextField();
		txtBoxCSVDirectory.setBounds(10, 151, 296, 20);
		contentPane.add(txtBoxCSVDirectory);
		txtBoxCSVDirectory.setColumns(10);
		
		JLabel lblEnterCSVDirectory = new JLabel("Enter File Directory Address");
		lblEnterCSVDirectory.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnterCSVDirectory.setBounds(10, 126, 296, 14);
		contentPane.add(lblEnterCSVDirectory);
		
		JLabel lblImportCSV = new JLabel("Import a CSV File");
		lblImportCSV.setHorizontalAlignment(SwingConstants.CENTER);
		lblImportCSV.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblImportCSV.setBounds(10, 40, 296, 41);
		contentPane.add(lblImportCSV);
	}
}
