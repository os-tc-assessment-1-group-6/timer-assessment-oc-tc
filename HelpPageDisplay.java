import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

public class HelpPageDisplay extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HelpPageDisplay frame = new HelpPageDisplay();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HelpPageDisplay() {
		setTitle("Countdown Timer");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 330, 370);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHelpPageTitle = new JLabel("Help Page");
		lblHelpPageTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblHelpPageTitle.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblHelpPageTitle.setBounds(10, 39, 296, 41);
		contentPane.add(lblHelpPageTitle);
	}
}
