/**
 * @author Selija Gangani
 *
 */
public class TimerDetails {

	private String description;
	private String time;
	private int positionID;

	public TimerDetails() {
		this.description = "";
		this.time = "";
		this.positionID = 0;
	}
	
	public TimerDetails(String description, String time, int positionID) {
		this.description = description;
		this.time = time;
		this.positionID = positionID;
	}
	
	public String getDescription() {
		return description;
	}


	public String getTime() {
		return time;
	}

	public int getPositionID() {
		return positionID;
	}

	@Override
	public String toString() {
		return "Description of Task: " + description + ", Time: " + time + ", PositionID: " + positionID;
	}
	
	public boolean equals(TimerDetails otherTimer) {
		if (this.description.equals(otherTimer.description) &&
			this.time == otherTimer.time &&
			this.positionID == otherTimer.positionID) {
			return true;
		}
		return false;	
	}

}
