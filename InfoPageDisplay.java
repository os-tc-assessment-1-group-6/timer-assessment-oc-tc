import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JList;

public class InfoPageDisplay extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InfoPageDisplay frame = new InfoPageDisplay();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InfoPageDisplay() {
		setTitle("Countdown Timer");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 330, 370);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblInfoPageTitle = new JLabel("Information Page");
		lblInfoPageTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblInfoPageTitle.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblInfoPageTitle.setBounds(10, 41, 296, 41);
		contentPane.add(lblInfoPageTitle);
		
		JLabel lblDevelopers = new JLabel("Developers");
		lblDevelopers.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDevelopers.setHorizontalAlignment(SwingConstants.CENTER);
		lblDevelopers.setBounds(10, 119, 296, 14);
		contentPane.add(lblDevelopers);
	}
}
