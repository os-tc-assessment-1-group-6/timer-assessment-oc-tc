# CountDown Timer Application - User Manual
 This application is designed to be used in a team meeting scenario.
 
## Features

- Specify sub-tasks and times
- Export and Import Sub-task and Time Configuration as a CSV file
- Utilise a Graphical or Browser-based User Interface Design
## Additional Features
- Visual table of activities
- Update and Delete activities from visual table
- Clear visual table (To be developed)

## How to Use Timer?
- 1. On the main page, click the Add Item button on the top menu.
- 2. Using the available textboxes enter the details of your first activity, then click the add button. 
- 3. Repeat step 2 if you wish to add more activities. 
- 4. To update an activity in the timer, select the activity from the visual table, re-enter the values into the available textboxes and click Update.
- 4. To delete an activity from the timer, select the activity from the visual table and click Delete.
-  Once all the activities have been added, you must export the activities to a CSV file 
-  5. Enter a filename for the activities to be exported to, once the Export button is clicked. 
- 6. Click the StartTimer button.
- 7. Click the Start button to start the countdown for the first activity.
- 8. Once the countdown has finished, click the Start button to begin the other activity.
- 9. Repeat step 8 until all the activities are finished.

## How to import a CSV file?
- 1. On the main page, click the File button then click Import CSV File.
- 2. Enter the file directory into the available textbox, click Import.
- 3. Continue from Step 5 in How to use Timer? instructions.

## Developers
- Darshi Gabani
- Selija Gangani
- Ryan Fletcher


