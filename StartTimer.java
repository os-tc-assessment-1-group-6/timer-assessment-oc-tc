import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.HeadlessException;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;

public class StartTimer extends JFrame {

	private JPanel contentPane;
	JLabel lblTimerDisplay;
	JLabel lblTimerDescription;
	JButton btnStartTimer;
	JButton btnPauseTimer;
	JLabel lblId;
	
	TimerDetails[] details;
	
	int i = 0;
	
	public StartTimer(TimerDetails[] details) {
		this();
		this.details = details;
		
		second = Integer.parseInt(details[i].getTime().substring(3));
		minute = Integer.parseInt(details[i].getTime().substring(0, 2));
		ddSecond = dF.format(second);
		ddMinute = dF.format(minute);
		lblTimerDisplay.setText(ddMinute + ":" + ddSecond);
		
		lblId.setText(Integer.toString(details[i].getPositionID()));
		lblTimerDescription.setText(details[i].getDescription());
		//change
		btnPauseTimer.setEnabled(false);
	}

	Timer timer;
	
	int second, minute;
	String ddSecond, ddMinute;
	
	
	DecimalFormat dF = new DecimalFormat("00");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartTimer frame = new StartTimer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StartTimer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 382, 465);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel contentPane_1 = new JPanel();
		contentPane_1.setLayout(null);
		contentPane_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane_1.setBounds(10, 11, 346, 402);
		contentPane.add(contentPane_1);
		
		lblTimerDisplay = new JLabel("00:00");
		lblTimerDisplay.setHorizontalAlignment(SwingConstants.CENTER);
		lblTimerDisplay.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblTimerDisplay.setBounds(10, 112, 327, 60);
		contentPane_1.add(lblTimerDisplay);
		
		lblTimerDescription = new JLabel("Timer Description");
		lblTimerDescription.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblTimerDescription.setHorizontalAlignment(SwingConstants.CENTER);
		lblTimerDescription.setBounds(10, 196, 327, 19);
		contentPane_1.add(lblTimerDescription);
		
		btnStartTimer = new JButton("Start");
		btnStartTimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 startTimer();
				 timer.start();
				 //change
				 btnStartTimer.setEnabled(true);
				 btnPauseTimer.setEnabled(true);
			}
		});
		btnStartTimer.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnStartTimer.setBounds(41, 278, 99, 48);
		contentPane_1.add(btnStartTimer);
		
		btnPauseTimer = new JButton("Pause");
		btnPauseTimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//change
				timer.stop();
				
				btnStartTimer.setEnabled(true);
				btnPauseTimer.setEnabled(false);
			}
		});
		btnPauseTimer.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnPauseTimer.setBounds(165, 278, 99, 48);
		contentPane_1.add(btnPauseTimer);
		
		lblId = new JLabel("");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblId.setHorizontalAlignment(SwingConstants.CENTER);
		lblId.setBounds(0, 30, 327, 28);
		contentPane_1.add(lblId);
	}
	
	private void startTimer() {
		// TODO Auto-generated method stub
		timer = new Timer(1000, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				second--;
				ddSecond = dF.format(second);
				ddMinute = dF.format(minute);
				lblTimerDisplay.setText(ddMinute + ":" + ddSecond);
				if (second==-1) {
				    second = 59;
					minute--;
					ddSecond = dF.format(second);
					ddMinute = dF.format(minute);
					lblTimerDisplay.setText(ddMinute + ":" + ddSecond);
				}
				if(minute==0 && second==0) {
					timer.stop();
					i++;
					
					second = Integer.parseInt(details[i].getTime().substring(3));
					minute = Integer.parseInt(details[i].getTime().substring(0, 2));
					ddSecond = dF.format(second);
					ddMinute = dF.format(minute);
					lblTimerDisplay.setText(ddMinute + ":" + ddSecond);
					
					lblId.setText(Integer.toString(details[i].getPositionID()));
					lblTimerDescription.setText(details[i].getDescription());
					
				}
			}
			
		});
	}
}