import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainDisplay extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainDisplay frame = new MainDisplay();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainDisplay() {
		setTitle("Countdown Timer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 330, 370);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(new Font("Segoe UI", Font.PLAIN, 10));
		menuBar.setBounds(0, 0, 316, 34);
		contentPane.add(menuBar);
		
		JMenu menuFile = new JMenu("File");
		menuFile.setForeground(Color.BLACK);
		menuFile.setFont(new Font("Segoe UI", Font.BOLD, 12));
		menuFile.setBackground(Color.WHITE);
		menuBar.add(menuFile);
		
		JMenuItem menuItemImportCVS = new JMenuItem("Import CSV File");
		menuItemImportCVS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ImportCSV ImportCSV = new ImportCSV();
				ImportCSV.setVisible(true);
			}
		});
		menuItemImportCVS.setSelected(true);
		menuFile.add(menuItemImportCVS);
		
		JMenuItem menuItemHelp = new JMenuItem("Help");
		menuItemHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HelpPageDisplay HelpPage = new HelpPageDisplay();
				HelpPage.setVisible(true);
			}
		});
		menuFile.add(menuItemHelp);
		
		JMenuItem menuItemInfo = new JMenuItem("Info");
		menuItemInfo.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				InfoPageDisplay InfoPage = new InfoPageDisplay();
				InfoPage.setVisible(true);
			}
		});
		menuFile.add(menuItemInfo);
		
		JMenuItem menuItemAdd = new JMenuItem("Add Item");
		menuBar.add(menuItemAdd);
		
		JMenuItem menuItemRemove = new JMenuItem("Remove Item");
		menuItemRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				RemoveItemDisplay RemoveItem = new RemoveItemDisplay();
				RemoveItem.setVisible(true);
			}
		});
		menuBar.add(menuItemRemove);
		
		menuItemAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AddItemDisplay AddItem = new AddItemDisplay();
				AddItem.setVisible(true);
				
			}
		});
		
		JLabel lblTimerDisplay = new JLabel("00:00");
		lblTimerDisplay.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblTimerDisplay.setBounds(112, 73, 77, 60);
		contentPane.add(lblTimerDisplay);
		
		JLabel lblTimerDescription = new JLabel("Timer Description");
		lblTimerDescription.setHorizontalAlignment(SwingConstants.CENTER);
		lblTimerDescription.setBounds(91, 132, 122, 14);
		contentPane.add(lblTimerDescription);
		
		JButton btnStartTimer = new JButton("Start");
		btnStartTimer.setBounds(41, 228, 99, 48);
		contentPane.add(btnStartTimer);
		
		JButton btnPauseTimer = new JButton("Pause");
		btnPauseTimer.setBounds(167, 228, 99, 48);
		contentPane.add(btnPauseTimer);
	}
}
