import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import java.awt.Color;
import javax.swing.JScrollPane;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ItemDisplay extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private DefaultTableModel model;
	JButton btnExport;
	JButton btnStartTimerButton;
	JButton btnAddItem;
	JButton btnUpdate;
	JButton btnDelete;

	Object[] row = new Object[3];

	private TimerDetails[] timer;
	private JTextField txtfilename;
	private FileWriter filewriter;
	private JTextField txtid;
	private JTextField txtdes;
	private JTextField txtduration;

	public ItemDisplay(TimerDetails[] timer) {							//Made a parametrised constructor which accepts the array of TimerDetails class
		this();
		this.timer = timer;

		addToTable();

	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ItemDisplay frame = new ItemDisplay();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ItemDisplay() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 767, 533);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 733, 485);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Display Activity");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblNewLabel.setBounds(170, 22, 563, 20);
		panel.add(lblNewLabel);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(270, 76, 420, 328);
		panel.add(scrollPane);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
		        int selectedIndex =table.getSelectedRow();
		        
		        txtid.setText(model.getValueAt(selectedIndex, 0).toString());
		        txtdes.setText(model.getValueAt(selectedIndex, 1).toString());
		        txtduration.setText(model.getValueAt(selectedIndex, 2).toString());
		        btnAddItem.setEnabled(false);
		        btnExport.setEnabled(false);
		        btnStartTimerButton.setEnabled(false);
		        btnUpdate.setEnabled(true);
		        btnDelete.setEnabled(true);
			}
		});
		model = new DefaultTableModel();
		Object[] column = {"Activity Id" , "Description", "Duration" };

		model.setColumnIdentifiers(column);
		table.setModel(model);
		table.setBackground(Color.WHITE);
		scrollPane.setViewportView(table);

		btnStartTimerButton = new JButton("Start Timer");
		btnStartTimerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new StartTimer(timer).setVisible(true);
				
			}
		});
		btnStartTimerButton.setBounds(270, 415, 115, 23);
		panel.add(btnStartTimerButton);

		btnExport = new JButton(" Export to CSV");
		btnExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exportToCSV();
			}

		});
		btnExport.setBounds(575, 449, 115, 25);
		panel.add(btnExport);

		txtfilename = new JTextField();
		txtfilename.setBounds(594, 416, 96, 20);
		panel.add(txtfilename);
		txtfilename.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Enter the File Name :");
		lblNewLabel_1.setBounds(480, 415, 104, 25);
		panel.add(lblNewLabel_1);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 76, 239, 372);
		panel.add(panel_1);
		panel_1.setLayout(null);

		btnAddItem = new JButton("Add");
		btnAddItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addItem();
			}
		});
		btnAddItem.setBounds(68, 267, 96, 23);
		panel_1.add(btnAddItem);
		
		JLabel lblEnterID = new JLabel("Enter Activity ID");
		lblEnterID.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnterID.setBounds(0, 80, 238, 14);
		panel_1.add(lblEnterID);
		
		txtid = new JTextField();
		txtid.setColumns(10);
		txtid.setBounds(68, 105, 96, 20);
		panel_1.add(txtid);

		JLabel lblEnterDescription = new JLabel("Enter Description");
		lblEnterDescription.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnterDescription.setBounds(0, 136, 238, 14);
		panel_1.add(lblEnterDescription);
		
		txtdes = new JTextField();
		txtdes.setColumns(10);
		txtdes.setBounds(68, 159, 96, 20);
		panel_1.add(txtdes);

		JLabel lblEnterDuration = new JLabel("Enter Duration");
		lblEnterDuration.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnterDuration.setBounds(0, 188, 238, 14);
		panel_1.add(lblEnterDuration);
		
		txtduration = new JTextField();
		txtduration.setColumns(10);
		txtduration.setBounds(68, 213, 96, 20);
		panel_1.add(txtduration);

		JLabel lblAddItemTitle = new JLabel("Add Activities to Timer");
		lblAddItemTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddItemTitle.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAddItemTitle.setBounds(0, 11, 238, 32);
		panel_1.add(lblAddItemTitle);
		
		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateitem();
			}
		});
		btnUpdate.setBounds(68, 301, 96, 23);
		panel_1.add(btnUpdate);
		
		btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteitem();
			}
		});
		btnDelete.setBounds(68, 335, 96, 26);
		panel_1.add(btnDelete);
		
		//change
		if (table.getRowCount() == 0) {
			btnExport.setEnabled(false);
			btnDelete.setEnabled(false);
			btnStartTimerButton.setEnabled(false);
			btnUpdate.setEnabled(false);
		}

	}
	private void exportToCSV() {

		if (table.getRowCount() == 0) {
			JOptionPane.showMessageDialog(this, "Unable to Export");

		} else if (txtfilename.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Please enter file name");
		} else {

			String path = System.getProperty("user.dir");
			path = path + "\\" + txtfilename.getText() + ".csv";

			try {
				filewriter = new FileWriter(path);

				DefaultTableModel model = (DefaultTableModel) table.getModel();

				timer = new TimerDetails[model.getRowCount()];

				for (int i = 0; i < model.getRowCount(); i++) {
					filewriter.append(model.getValueAt(i, 0).toString());
					filewriter.append(",");
					filewriter.append(model.getValueAt(i, 1).toString());
					filewriter.append(",");
					filewriter.append(model.getValueAt(i, 2).toString());
					filewriter.append("\n");
					
					timer[i] = new TimerDetails(model.getValueAt(i, 1).toString(), model.getValueAt(i, 2).toString(), Integer.parseInt(model.getValueAt(i, 0).toString()));
					//change
					int second = Integer.parseInt(model.getValueAt(i, 2).toString().substring(3));
					int minute = Integer.parseInt(model.getValueAt(i, 2).toString().substring(0, 2));
				}

				JOptionPane.showMessageDialog(this, "File exported at : " + path);
				btnExport.setEnabled(false);
				btnAddItem.setEnabled(false);
				btnStartTimerButton.setEnabled(true);
				btnDelete.setEnabled(false);
				btnUpdate.setEnabled(false);

			} catch (IOException e) {
				JOptionPane.showMessageDialog(this, "Unable to create file.");

			}//change
			catch (Exception e) {
				JOptionPane.showMessageDialog(this, "Type mismatch in data " +e.getMessage());
			} 
			//done
			finally {
				try {
					filewriter.flush();
					filewriter.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

	private void addToTable() {

		for (int i = 0; i < timer.length; i++) {
			row[0] = timer[i].getPositionID();
			row[1] = timer[i].getDescription();
			row[2] = timer[i].getTime();
			model.addRow(row);
		}

		btnExport.setEnabled(false);
		btnStartTimerButton.setEnabled(true);
	}

	private void addItem() {
		if (txtid.getText().equals("") ||txtdes.getText().equals("") || txtduration.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Please fill all the fields!");
		} else {

			row[0] = txtid.getText();
			row[1] = txtdes.getText();
			row[2] = txtduration.getText();
			model.addRow(row);
			//JOptionPane.showMessageDialog(this, "Activity added.");
			
			txtid.setText("");
			txtdes.setText("");
			txtduration.setText("");

			btnStartTimerButton.setEnabled(false);
			btnExport.setEnabled(true);

			btnUpdate.setEnabled(true);
			btnDelete.setEnabled(true);
		
		}
	}
	private void updateitem() {
		// TODO Auto-generated method stub
		if (txtid.getText().equals("") ||txtdes.getText().equals("") || txtduration.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Please fill all the fields!");
		} else {
			DefaultTableModel d1 = (DefaultTableModel) table.getModel();
	        int selectedIndex =table.getSelectedRow();
	        
			row[0] = txtid.getText();
			row[1] = txtdes.getText();
			row[2] = txtduration.getText();
			
			model.setValueAt(row[0], selectedIndex , 0);
			model.setValueAt(row[1], selectedIndex, 1);
			model.setValueAt(row[2], selectedIndex, 2);
			JOptionPane.showMessageDialog(this, "Activity updated.");
			
			txtid.setText("");
			txtdes.setText("");
			txtduration.setText("");

			btnStartTimerButton.setEnabled(false);
			btnExport.setEnabled(true);
			//change
			table.clearSelection();
			btnAddItem.setEnabled(true);

		}
		
	}
	private void deleteitem() {
		
		DefaultTableModel d1 = (DefaultTableModel) table.getModel();
        int selectedIndex =table.getSelectedRow();
        d1.removeRow(selectedIndex);
        JOptionPane.showMessageDialog(this, "Activity deleted.");
        
		txtid.setText("");
		txtdes.setText("");
		txtduration.setText("");

		btnStartTimerButton.setEnabled(false);
		btnExport.setEnabled(true);
		//change
		table.clearSelection();
		btnAddItem.setEnabled(true);
	}
}